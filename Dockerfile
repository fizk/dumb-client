# This image is to build the React client side bundle file
# It is based off of a Node image. 
# First it copies package.json into the workspace and then install all
# dependencies.
# Next it copies all the config files and lastly it builds everything into
# the build directory (configured in Webpack).
FROM node:14.11.0-stretch AS builder

WORKDIR /app

COPY ./package*.json /app/

RUN npm i

COPY ./src /app/src
COPY ./tsconfig.json /app/tsconfig.json
COPY ./.babelrc /app/.babelrc
COPY ./webpack.config.js /app/webpack.config.js
COPY ./index.html /app/index.html

RUN npm run build


# This is the actual server, it is based off of an Apache image.
# First it configures all redirect and proxy rules.
# Next it copies only the React build file from the previous Docker build
# into `httpd` leaving all node_modules behind as they are not needed.
FROM httpd:2.4.46 AS server

RUN echo "<Directory \"/usr/local/apache2/htdocs\">\n \
<IfModule mod_rewrite.c> \n \
    RewriteEngine On\n \
    RewriteBase /\n \
    RewriteRule ^index\.html$ - [L]\n \
    RewriteCond %{REQUEST_FILENAME} !-f\n \
    RewriteCond %{REQUEST_FILENAME} !-d\n \
    RewriteRule . /index.html [L]\n \
</IfModule>\n \
</Directory>\n\n \
ProxyPass /api http://drupal:80" >> /usr/local/apache2/conf/httpd.conf

RUN sed -i 's/\#LoadModule proxy_module/LoadModule proxy_module/g' /usr/local/apache2/conf/httpd.conf
RUN sed -i 's/\#LoadModule proxy_http_module/LoadModule proxy_http_module/g' /usr/local/apache2/conf/httpd.conf
RUN sed -i 's/\#LoadModule rewrite_module/LoadModule rewrite_module/g' /usr/local/apache2/conf/httpd.conf

COPY --from=builder /app/index.html /usr/local/apache2/htdocs/index.html
COPY --from=builder /app/build /usr/local/apache2/htdocs/scripts