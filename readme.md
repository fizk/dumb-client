## The idea
Since React applications run in the browser and only need one bundled file (usually called bundle.js), it makes sense to create a Docker container that is based off of a HTTP server that contains only this static JS file.

But because React is written in such a way that the browser can't run it (JSX), the React application needs to be compiled and bundled up.

Therefor this Dockerfile is a two-stage file.

First it extends a Node image that copies all config files and the source-code before it compiles and bundled the React app into a single file.

Next a new image is created based off of an Apache image, that copies the artifacts from the last build into its `httpd` directory. Now everything is ready to be deployed.

Not quite...

Single-Page-Application need all their URIs to be routed to `index.html`. The Apache image is therefore configured so that all URIs pointed to it, will always serve up index.html

And that is it for production.

## Production
This will produce a single **"baked"** Docker container that contains all the artifacts needed to run a Single-Page-Application plus a configured HTTP server that can serve the artifacts to a browser. This container is therefor production ready.

Summary:
To run this image in production simply run:
```
docker build -t server . && docker run -p 80:80 -d server
```

## Development
You don't want to rebuild this image every time you make changes to the code in development. Therefor an docker-compose.yaml file is included for development. It will run the Dockerfile from the production step, but then it will mount the **host** `build` folder into `./httpd/scripts` inside the the Apache container along with the `index.html` file.

A developer then just runs **Webpack** in watch mode and the Apache will serve up the latest `bundle.js` file.

Summary:
To run in development:
```
npm i
docker-compose up -d dev && npm run dev
```