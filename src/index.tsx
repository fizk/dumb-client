import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import type { FunctionComponent } from 'react';

interface Props {}

const App: FunctionComponent<Props> = () => {
	const [state, updateState] = useState<string|null>(null);
	const [node, updateNode] = useState({});
	const fetchNode = () => {
		fetch(`api/node/${state}?_format=json`)
			.then(response =>  response.json())
			.then(updateNode)
	}
	return (
		<>
			<h1>Hello World</h1>
			<div>
				<p>
					show me node #no
					<input onChange={event => updateState(event.target.value)} />
					<button onClick={fetchNode}>click me</button>
				</p>
			</div>
			<pre>{JSON.stringify(node, undefined, 4)}</pre>
		</>
	);
}

ReactDOM.render(
	<App />,
	document.querySelector('[data-react]')
);